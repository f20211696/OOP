import java.lang.Math;

class Record {
    private long key;
    private double value;

    public Record(double val) {
        this.value = val;
        this.key = Math.round(val);
    }

    public long GetKey() {
        return this.key;
    }

    public double GetValue() {
        return this.value;
    }
}

class Dictionary {
    private Record[] records;
    private int size;
    private int length;
    enum ordering {NONE,ASC,DESC,PRIQASC,PRIQDESC,BST};

    public Dictionary(int initialsize) {
        records = new Record[initialsize];
        size = initialsize;
        length = 0;
    }

    public int size() {
        return records.length;
    }
    
    public Record put(Record record) {
        for (int i=0; i<this.length; i++) {
            if ((this.records)[i].GetKey() == record.GetKey()) {
                Record r = (this.records)[i];
                (this.records)[i] = record;
                return r;
            }
        }
        (this.records)[this.length] = record;
        this.length++;
        return null;
    }

    public double get(int key) {
        for (int i=0; i<length; i++) {
            if ((this.records)[i].GetKey() == key) {
                return (this.records)[i].GetValue();
            }
        }
        return 0.0;
    }

    public Record remove(int key) {
        for (int i=0; i<length; i++) {
            if ((this.records)[i].GetKey() == key) {
                records[i] = null;
                length--;
                return (this.records)[i];
            } 
        }
        return null;
    }

    public boolean isEmpty() {
        if (length == 0) {
            return true;
        }
        return false;
    }

    public boolean isFull() {
        if (length == size) {
            return true;
        }
        return false;
    }

    public Record[] records() {
        return records;
    }
    
    public long[] keys() {
        long[] k = new long[length];
        for (int i=0; i < length; i++) {
            k[i] = records[i].GetKey();
        }
        return k;
    }

    public void Show() {
        for (int i=0; i<this.length; i++) {
            System.out.print(" " + records[i].GetKey() + ":" + records[i].GetValue());
        }
        for (int i=length; i<size; i++) {
            System.out.print(" .");
        }
    }
}

class DictionaryApp {
    public static void main (String[] args) {
        double[] values;
        int size, count;
        size=Integer.parseInt(args[0]);
        count=Integer.parseInt(args[1]);
        values = new double[size];
        String[] elems = java.util.Arrays.copyOfRange(args, 2, args.length);
        for (int i=0; i<count; i++) {
            values[i] = Double.parseDouble(elems[i]);
        }
        Dictionary d = new Dictionary(size);
        for (int i=0; i<count; i++) {            
            d.put(new Record(values[i]));
        }
        d.Show();
        d.remove(2);
        d.Show();
    }
}